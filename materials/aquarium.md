---
title: Aquarium Experiment
---

# Aquarium Experiment

The Atlantic cod growth model described here is a single-species bio-physical growth model that simulates temperature- and size-dependent growth of Atlantic cod (Gadus morhua). The model is derived from experimental data (Björnsson and Steinarsson, 2002; Björnsson et al., 2007) by Butzin and Pörtner (2016). The model assumes that Atlantic cod growth rates are mainly temperature- and size-dependent*. The model simulates daily growth of cod which mimics controlled laboratory experiments. 

The model results cover the temperature range from 2 to 16 °C (T 2, 4, 7, 8, 10, 12, 13, and 16 °C), and the body weight range 0.001–8 kg (Björnsson and Steinarsson, 2002; Björnsson et al., 2007).
Core model assumptions:

- the rate at which an organism grows depends on the value of its own body mass, i.e. allometric growth (White and Kearney, 2014);
- an organism responds to temperature changes immediately;
- larval growth variations, vertical, or horizontal movements and ontogenetic habitat shifts are not considered;
- individuals grow under unlimited food supply and in a homogeneous thermal environment (based on data from Björnsson and Steinarsson, 2002; Björnsson et al., 2007).

